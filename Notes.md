# General


Change **Action Cards** into -> **Motion** cards ("**Motion** deck", "playing a **Motion**", "opposing a **Motion** with a **Veto** card", etc...)


## - Title

- Tyrants and Tribunes


## - Links

>https://en.wikipedia.org/wiki/Optimates_and_populares

>https://en.wikipedia.org/wiki/List_of_Roman_laws

>https://en.wikipedia.org/wiki/Roman_emergency_decrees

>https://en.wikipedia.org/wiki/Senatus_consultum_ultimum

>https://en.wiktionary.org/wiki/optimate

>https://en.wiktionary.org/wiki/populare

>https://en.wikipedia.org/wiki/Delator

>https://www.britannica.com/topic/delict




# Number of Players


## - Faction & Role Cards

|Players |Senators |Tribunes |Moderates |Tyrants |
|:------:|:-------:|:-------:|:--------:|:------:|
|4       |         |         |          |        |
|5       |2-3*     |2-3*     |3         |2       |
|6       |         |         |          |        |
|7       |         |         |          |        |
|8       |         |         |          |        |
|9       |         |         |          |        |
|10      |         |         |          |        |
|11      |         |         |          |        |
|12      |         |         |          |        |


## - Action Cards

|Players |Optimate L. |Populare L. |Ultimum D. |Veto |Filibuster |Delict |Delator |Proscription |
|:------:|:----------:|:----------:|:---------:|:---:|:---------:|:-----:|:------:|:-----------:|
|4       | 			  |			   |		   |	 |	 		 |	     |	      |				|
|5       |8 		  |8 		   |6 		   |2 	 |2 		 |2 	 |2 	  |2 			|
|6       | 			  |			   |		   |	 |	 		 |	     |	      |				|
|7       | 			  |			   |		   |	 |	 		 |	     |	      |				|
|8       | 			  |			   |		   |	 |	 		 |	     |	      |				|
|9       | 			  |			   |		   |	 |	 		 |	     |	      |				|
|10      | 			  |			   |		   |	 |	 		 |	     |	      |				|
|11      | 			  |			   |		   |	 |	 		 |	     |	      |				|
|12      | 			  |			   |		   |	 |	 		 |	     |	      |				|


## - Victory Conditions

|Players |Optimates   |Populares   |Tyrant 1     |Tyrant 2     |
|:------:|:----------:|:----------:|:-----------:|:-----------:|
|4       | 			  |			   |	    	 |		       |
|5       |6 	      |6		   |3		     |5		       |
|6       | 			  |			   |		     |		       |
|7       | 			  |			   |		     |		       |
|8       | 			  |			   |		     |		       |
|9       | 			  |			   |		     |		       |
|10      | 			  |			   |		     |		       |
|11      | 			  |			   |		     |		       |
|12      | 			  |			   |		     |		       |




# Check Out - Inspirations


>https://boardgamegeek.com/boardgame/271601/feed-kraken

>https://boardgamegeek.com/boardgame/320718/hidden-leaders

>https://boardgamegeek.com/boardgame/130877/blood-bound

>https://boardgamegeek.com/boardgame/24068/shadow-hunters

>https://boardgamegeek.com/boardgame/143741/bang-dice-game

>https://boardgamegeek.com/boardgame/131357/coup

>https://boardgamegeek.com/boardgame/188188/complots

>https://boardgamegeek.com/boardgame/128882/resistance-avalon

>https://boardgamegeek.com/boardgame/41114/resistance

>https://boardgamegeek.com/boardgame/240980/blood-clocktower

>https://boardgamegeek.com/boardgame/147949/one-night-ultimate-werewolf

>https://boardgamegeek.com/boardgame/230059/crossfire

>https://boardgamegeek.com/boardgame/403000/traitors-aboard

>https://boardgamegeek.com/boardgame/139030/mascarade

>https://boardgamegeek.com/boardgame/299169/spicy

>https://boardgamegeek.com/boardgame/156129/deception-murder-hong-kong

>https://boardgamegeek.com/boardgame/15062/shadows-over-camelot

>https://boardgamegeek.com/boardgame/176558/mafia-de-cuba

>https://boardgamegeek.com/boardgame/150376/dead-winter-crossroads-game

>https://boardgamegeek.com/boardgame/9220/saboteur

>https://boardgamegeek.com/boardgame/298638/sheriff-nottingham-2nd-edition

>https://boardgamegeek.com/boardgame/273477/obscurio

>https://boardgamegeek.com/boardgame/356909/hand-hand-wombat




# Art Generation Prompts


- Painting of an ancient roman legionnaire arresting an ancient roman senator wearing a toga.


- Painting of an ancient roman senator wearing a toga talking passionately in front of a tribune.


- Painting of an ancient roman senator wearing a black toga talking passionately in front of a tribune.


- Painting of an ancient roman senator wearing a toga pointing his finger while accusing another guilty senator.


- Painting of an ancient roman senator wearing a toga raising their hand to oppose a motion.




# Legacy


## - Cards

- **Veto** : Choose a player and skip their next turn.

- **Proscription** : Choose a player. If the majority agrees, remove that player from the game and don't reveal any of their cards.

- **Kalends** : Choose a player to take the next turn, and the game continues from there.


## - Names

- Magistrate

- Quorum

- Ambitus
